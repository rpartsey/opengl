#/bin/bash

export IMAGE_NAME="fairembodied/cudagl:11.7.1-cudnn8-devel"
export LIBGLVND_VERSION="1.2"

export FROM="nvidia/cuda:11.7.1-cudnn8-devel-ubuntu22.04"
export OS="ubuntu"
export OS_VERSION="22.04"

cp NGC-DL-CONTAINER-LICENSE base/

echo -e ">>> Building BASE Image\n"

docker build -t "${IMAGE_NAME}-base-${OS}${OS_VERSION}" --build-arg "from=${FROM}" "base/"

echo -e "\n>>> Building RUNTIME Image\n"

docker build -t "${IMAGE_NAME}-${LIBGLVND_VERSION}-glvnd-runtime-${OS}${OS_VERSION}" \
             --build-arg "from=${IMAGE_NAME}-base-${OS}${OS_VERSION}" \
             --build-arg "LIBGLVND_VERSION=${LIBGLVND_VERSION}" \
             "glvnd/runtime"

echo -e "\n>>> Building DEVEL Image\n"

docker build -t "${IMAGE_NAME}-${LIBGLVND_VERSION}-glvnd-devel-${OS}${OS_VERSION}" \
             --build-arg "from=${IMAGE_NAME}-${LIBGLVND_VERSION}-glvnd-runtime-${OS}${OS_VERSION}" \
             "glvnd/devel"
